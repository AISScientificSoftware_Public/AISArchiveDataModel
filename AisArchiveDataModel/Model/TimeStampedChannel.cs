﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary>
    ///     A time stamped channel that contains <see cref="TimeStampedSample{TData}"/> data.
    /// </summary>
    ///
    /// <remarks>
    ///     Time stamped channels contain time staped data; i.e., each data sample has an associated
    ///     sample time. This is generally useful for channels that are not regularly sampled, or
    ///     where the sample period can differ slightly between samples.
    /// </remarks>
    ///
    /// <typeparam name="TData"> Type of the data. </typeparam>
    /// 
    /// ### <inheritdoc/>
    public class TimeStampedChannel<TData> : ChannelBase, IChannel<TData>
    {
        #region IChannel

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable<TimeSpan> TimeStamps => TimeStampedSamples.Select(s => s.Time);

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable Data => TimeStampedSamples;

        #endregion

        #region IChannel<TData>

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable<TData> Values => TimeStampedSamples.Select(s => s.Value);

        /// <inheritdoc />
        public IEnumerable<TimeStampedSample<TData>> TimeStampedSamples { get; }
        
        #endregion

        #region Constructors    
        
        [JsonConstructor]
        public TimeStampedChannel(
            string name,
            UnitOfMeasurement unitOfMeasurement,
            IEnumerable<TimeStampedSample<TData>> timeStampedSamples)
            : base(name, unitOfMeasurement)
        {
            TimeStampedSamples = timeStampedSamples;
        }

        public TimeStampedChannel(
            string name,
            Enum unitOfMeasurement,
            IEnumerable<TimeStampedSample<TData>> timeStampedSamples)
            : this(name, new UnitOfMeasurement(unitOfMeasurement), timeStampedSamples)
        {
        }
        
        #endregion
    }
}