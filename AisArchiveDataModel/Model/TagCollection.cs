﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> Collection of tags. </summary>
    ///
    /// <remarks>
    ///     The collection acts just like a Dictionary, but imposes some restrictions on the values
    ///     that may be added to it. This makes it easier to reason about when validating the tags.
    /// </remarks>
    ///
    /// ### <inheritdoc/>
    public class TagCollection : IDictionary<string,object>
    {
        private readonly Dictionary<string, object> _tags = new Dictionary<string, object>();

        #region IDictionary<string, object>

        /// <summary>
        ///     Adds a tag with a specified key and value.
        /// </summary>
        ///
        /// <remarks> Adds tag, checking that the key and value are valid. </remarks>
        ///
        /// <param name="key"> The object to use as the key of the element to add. </param>
        /// <param name="value"> The object to use as the value of the element to add. </param>
        ///
        /// <inheritdoc/>
        public void Add(string key, object value)
        {
            if (value == null) throw new ArgumentNullException(nameof(value));
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException(nameof(key));
            if (value is string s && string.IsNullOrWhiteSpace(s)) throw new ArgumentException(nameof(value));

            _tags.Add(key, value);
        }

        IEnumerator<KeyValuePair<string, object>> IEnumerable<KeyValuePair<string, object>>.GetEnumerator() =>
            _tags.GetEnumerator();

        public IEnumerator GetEnumerator() => _tags.GetEnumerator();

        public bool ContainsKey(string key) => _tags.ContainsKey(key);

        public bool Remove(string key) => _tags.Remove(key);

        public bool TryGetValue(string key, out object value) => _tags.TryGetValue(key, out value);

        public object this[string key]
        {
            get => _tags[key];
            set => _tags[key] = value;
        }

        public ICollection<string> Keys => _tags.Keys;

        public ICollection<object> Values => _tags.Values;

        public void Clear() => _tags.Clear();

        public void Add(KeyValuePair<string, object> item) => _tags.Add(item.Key, item.Value);

        public bool Contains(KeyValuePair<string, object> item) => _tags.Contains(item);

        public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex) =>
            (_tags as ICollection<KeyValuePair<string, object>>).CopyTo(array, arrayIndex);

        public bool Remove(KeyValuePair<string, object> item) =>
            (_tags as ICollection<KeyValuePair<string, object>>).Remove(item);

        public int Count => _tags.Count;

        public bool IsReadOnly => (_tags as ICollection<KeyValuePair<string, object>>).IsReadOnly;
        
        #endregion
    }
}
