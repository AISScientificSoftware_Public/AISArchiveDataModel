﻿using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A device from which measurements are made. </summary>
    public class Device : ArchiveBase
    {
        /// <summary> Gets or sets the manufacturer. </summary>
        ///
        /// <remarks> Manufacturer should be the company name; e.g., Catapult. </remarks>
        /// 
        /// <value> The manufacturer. </value>
        public string Manufacturer { get; set; }

        /// <summary> Gets or sets the name of the model. </summary>
        ///
        /// <value> The name of the model. </value>
        public string ModelName { get; set; }

        /// <summary> Gets or sets the internal identifier of the device. </summary>
        ///
        /// <remarks>
        ///     The internal identifier is a non-physical identifier that is used to identify the device,
        ///     often in software. For example, a device's API could return an identifier to a device
        ///     that is different from the device's serial number.
        /// </remarks>
        ///
        /// <value> The internal identifier. </value>
        public string InternalIdentifier { get; set; }

        /// <summary> Gets or sets the external identifier of the device. </summary>
        ///
        /// <remarks> A physical identifier on the device; e.g., serial number. </remarks>
        /// 
        /// <value> The external identifier. </value>
        public string ExternalIdentifier { get; set; }

        /// <summary> Gets or sets the firmware version. </summary>
        ///
        /// <remarks> Example: 1.0.0. </remarks>
        ///
        /// <value> The firmware version. </value>
        public string FirmwareVersion { get; set; } // e.g., 1.0

        /// <summary> Gets or sets the actor. </summary>
        ///
        /// <remarks>
        ///     The actor represents the subject of the device's measurements; e.g., an athlete, or a
        ///     boat. If a single physical device acts on multiple actors then the device should be split
        ///     into multiple "virtual devices", each of which acts on a single actor.
        /// </remarks>
        ///
        /// <value> The actor. </value>
        public Actor Actor { get; set; }

        /// <summary> Gets or sets the sensors. </summary>
        ///
        /// <value> The measurement sensors that are part of the device. </value>
        public IEnumerable<Sensor> Sensors { get; set; }
    }
}