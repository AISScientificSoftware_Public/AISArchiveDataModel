﻿using System;
using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A session. </summary>
    ///
    /// <remarks>
    ///     Sessions represent the top-level of this data model hierarchy. Example sessions could
    ///     include a race, training session, or calibration trial.
    /// </remarks>
    public class Session : ArchiveBase
    {
        /// <summary> Gets or sets the name of the system that is recording the data. </summary>
        ///
        /// <remarks>
        ///     Systems represent the collection of measurement devices, and any associated hardware and
        ///     software that are responsible for recording data, analysing, and persisting it. Examples
        ///     include SPARTA, Wetplate, Indoor Track.
        /// </remarks>
        ///
        /// <value> The name of the system. </value>
        public string SystemName { get; set; }

        /// <summary> Gets or sets the system version. </summary>
        ///
        /// <remarks>
        ///     Whenever the hardware of software in a system is changed, the <see cref="SystemVersion"/>
        ///     should be changed to reflect that fact. This allows us to track behaviour across
        ///     different versions of the system, as well as track down bugs. It also allows us to run
        ///     version-specific validation or analysis on system data.
        ///     
        ///     Semantic Versioning is recommended (see: http://semver.org).
        ///     
        ///     Example: 1.0.0
        /// </remarks>
        ///
        /// <value> The system version. </value>
        public string SystemVersion { get; set; }

        /// <summary> Gets or sets the identifier of the session. </summary>
        ///
        /// <remarks>
        ///     The <see cref="SessionId"/> is a system-specific value.
        /// </remarks>
        /// 
        /// <value> The identifier of the session. </value>
        public string SessionId { get; set; }

        /// <summary> Gets or sets the start time and date of the session. </summary>
        ///
        /// <value> The start time. </value>
        public DateTime StartTime { get; set; }

        /// <summary> Gets or sets the end time and data of the session. </summary>
        ///
        /// <value> The end time. </value>
        public DateTime? EndTime { get; set; }

        /// <summary> Gets or sets the session location. </summary>
        ///
        /// <value> The session location. </value>
        public Location SessionLocation { get; set; }

        /// <summary> Gets or sets the comment. </summary>
        ///
        /// <value> The comment. </value>
        public string Comment { get; set; }

        /// <summary> Gets or sets the devices used in the system/session. </summary>
        ///
        /// <value> The devices. </value>
        public IEnumerable<Device> Devices { get; set; }

        /// <summary> Gets or sets the events that occurred in the session. </summary>
        ///
        /// <value> The events. </value>
        public IEnumerable<Annotation> Events { get; set; }
    }
}