﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A time-series data channel for regularly sampled data. </summary>
    ///
    /// <remarks>
    ///     This class is used to represent data that has been sampled at regular intervals in time.
    ///     Note that the channel can only have a single <see cref="UnitOfMeasurement" />. This means
    ///     that <see cref="TData" /> cannot be a composite type that contains measurements in
    ///     different units. <see cref="TData" /> could, however, represent a composite type that has
    ///     the same unit of measurement for each element; e.g., acceleration {x, y, z}.
    /// </remarks>
    ///
    /// <typeparam name="TData"> Type of the data. </typeparam>
    /// 
    /// ### <inheritdoc/>
    /// 
    public class SampledChannel<TData> : ChannelBase, IChannel<TData>
    {
        #region Timing information

        /// <summary> Gets or sets the sample rate, in Hz. </summary>
        ///
        /// <value> The sample rate, in Hz. </value>
        public double SampleRateHz { get; }

        /// <summary> Gets or sets the start time offset. </summary>
        ///
        /// <remarks>
        ///     The start time offset is used to record the offset in time between the start of a
        ///     <see cref="Session"/> and the first sample in the channel. If the first sample is
        ///     recorded at the start of the session, set <see cref="StartTimeOffset"/> to TimeSpan.Zero.
        ///     Alternatively, you can leave it as unset, as TimeSpan.Zero is the default value.
        /// </remarks>
        ///
        /// <value> The start time offset. </value>
        public TimeSpan StartTimeOffset { get; set; }

        #endregion

        #region IChannel

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable<TimeSpan> TimeStamps =>
            Enumerable
                .Range(0, Values.Count())
                .Select((x, i) => GetSampleTime(i));

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable Data => Values;

        #endregion

        #region IChannel<TData>

        /// <inheritdoc />
        public IEnumerable<TData> Values { get; }

        /// <inheritdoc />
        [JsonIgnore]
        public IEnumerable<TimeStampedSample<TData>> TimeStampedSamples =>
            Values
                .Select((data, i) => new TimeStampedSample<TData> {
                    Time = GetSampleTime(i),
                    Value = data
                });
        
        #endregion

        #region Constructors

        [JsonConstructor]
        public SampledChannel(
            string name,
            UnitOfMeasurement unitOfMeasurement,
            double sampleRateHz,
            IEnumerable<TData> values)
            : base(name, unitOfMeasurement)
        {
            SampleRateHz = sampleRateHz;
            Values = values;
        }

        public SampledChannel(
            string name,
            Enum unitOfMeasurement,
            double sampleRateHz,
            IEnumerable<TData> data)
            : this(name, new UnitOfMeasurement(unitOfMeasurement), sampleRateHz, data)
        {
        }

        private TimeSpan GetSampleTime(int sampleIndex) =>
            StartTimeOffset + TimeSpan.FromSeconds(sampleIndex / SampleRateHz);
    }

    #endregion
}