﻿using System;

namespace gov.ausport.AisArchiveDataModel
{
    public class ChannelBase : ArchiveBase
    {
        #region IChannel (part of implementation)

        /// <summary> Gets the name of the channel. </summary>
        ///
        /// <value> The name. </value>
        public string Name { get; }

        /// <summary> Gets the unit of measurement. </summary>
        ///
        /// <value> The unit of measurement. </value>
        public UnitOfMeasurement UnitOfMeasurement { get; }
        
        #endregion

        #region Constructors    
        
        protected ChannelBase(string name, UnitOfMeasurement unitOfMeasurement)
        {
            Name = !string.IsNullOrWhiteSpace(name)
                ? name
                : throw new ArgumentException(nameof(name));
            UnitOfMeasurement = unitOfMeasurement
                                ?? throw new ArgumentNullException(nameof(unitOfMeasurement));
        }
        
        #endregion
    }
}