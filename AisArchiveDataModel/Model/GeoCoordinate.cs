﻿using System;

namespace gov.ausport.AisArchiveDataModel
{
    public class GeoCoordinate
    {
        public static readonly (double Min, double Max) LongitudeRange = (-180, 180);
        public static readonly (double Min, double Max) LatitudeRange = (-90, 90);
        public static readonly double MinimumAccuracyValue = 0.0;

        public static readonly GeoCoordinate Unknown = new GeoCoordinate(); // The "Unknown" GeoCoordinate

        /// <summary>
        ///     Gets a value that indicates whether the GeoCoordinate does not contain latitude or
        ///     longitude data.
        /// </summary>
        ///
        /// <remarks>
        ///     A GeoCoordinate that does not contain latitude or longitude data is equal to
        ///     <see cref="Unknown"/>.
        /// </remarks>
        ///
        /// <value> true if the GeoCoordinate does not contain latitude or longitude data; otherwise, false. </value>
        public bool IsUnknown => this == Unknown;

        /// <summary> Gets the latitude, in decimal degrees. </summary>
        ///
        /// <remarks>
        ///     Latitude can range from -90.0 to 90.0. Latitude is measured in degrees north or south
        ///     from the equator. Positive values are north of the equator and negative values are south
        ///     of the equator.
        /// </remarks>
        ///
        /// <value> Latitude of the location, in decimal degrees. </value>
        public double Latitude { get; } // decimal degrees

        /// <summary> Gets the longitude, in decimal degrees. </summary>
        ///
        /// <remarks>
        ///     The longitude can range from -180.0 to 180.0. Longitude is measured in degrees east or
        ///     west of the prime meridian. Negative values are west of the prime meridian, and positive
        ///     values are east of the prime meridian.
        /// </remarks>
        ///
        /// <value> The longitude of the location, in decimal degrees. </value>
        public double Longitude { get; }

        /// <summary> Gets the altitude in meters. </summary>
        ///
        /// <remarks> The altitude is given relative to sea level. </remarks>
        ///
        /// <value> The altitude, in meters. </value>
        public double? Altitude { get; } // meters

        /// <summary>
        ///     Gets the accuracy of the latitude and longitude that is given by the GeoCoordinate, in
        ///     meters.
        /// </summary>
        ///
        /// <remarks>
        ///     The accuracy can be considered the radius of certainty of the latitude and longitude
        ///     data. A circular area that is formed with the accuracy as the radius and the latitude and
        ///     longitude coordinates as the center contains the actual location.
        /// </remarks>
        ///
        /// <value> The accuracy of the latitude and longitude, in meters. </value>
        public double? HorizontalAccuracy { get; } // meters

        /// <summary> Gets the accuracy of the altitude given by the GeoCoordinate, in meters. </summary>
        ///
        /// <value> The accuracy of the altitude, in meters. </value>
        public double? VerticalAccuracy { get; } // meters

        /// <summary> Private constructor that constructs an "Unknown" GeoCoordinate. </summary>
        ///
        /// <remarks> Cannot call regular constructor as it would fail the validation checks. </remarks>
        private GeoCoordinate()
        {
            Longitude = double.NaN;
            Latitude = double.NaN;
        }

        /// <summary> Initializes a new instance of the GeoCoordinate class. </summary>
        ///
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        ///     illegal values. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///     the required range. </exception>
        ///
        /// <param name="longitude"> Longitude. May range from -180.0 to 180.0 decimal degrees. </param>
        /// <param name="latitude"> Latitude. May range from -90.0 to 90.0 decimal degrees. </param>
        /// <param name="altitude"> (Optional) The altitude, in meters. </param>
        /// <param name="horizontalAccuracy"> (Optional) The accuracy of the latitude and longitude, in
        ///     meters. Must be greater than zero. </param>
        /// <param name="verticalAccuracy"> (Optional) The accuracy of the altitude, in meters. Must be
        ///     greater than zero. </param>
        public GeoCoordinate(
            double longitude,
            double latitude,
            double? altitude = null,
            double? horizontalAccuracy = null,
            double? verticalAccuracy = null)
        {
            if (double.IsNaN(longitude)) throw new ArgumentException(nameof(longitude));
            if (double.IsNaN(latitude)) throw new ArgumentException(nameof(latitude));

            if(longitude < LongitudeRange.Min || longitude > LongitudeRange.Max)
                throw new ArgumentOutOfRangeException(nameof(longitude));
            if(latitude < LatitudeRange.Min || latitude > LatitudeRange.Max)
                throw new ArgumentOutOfRangeException(nameof(latitude));
            if(horizontalAccuracy.HasValue && horizontalAccuracy.Value < MinimumAccuracyValue)
                throw new ArgumentOutOfRangeException(nameof(horizontalAccuracy));
            if(verticalAccuracy.HasValue && verticalAccuracy.Value < MinimumAccuracyValue)
                throw new ArgumentOutOfRangeException(nameof(verticalAccuracy));

            if (altitude.HasValue && (double.IsNaN(altitude.Value) || double.IsInfinity(altitude.Value)))
                throw new ArgumentException(nameof(altitude));
            if (horizontalAccuracy.HasValue && (double.IsNaN(horizontalAccuracy.Value) || double.IsInfinity(horizontalAccuracy.Value)))
                throw new ArgumentException(nameof(horizontalAccuracy));
            if (verticalAccuracy.HasValue && (double.IsNaN(verticalAccuracy.Value) || double.IsInfinity(verticalAccuracy.Value)))
                throw new ArgumentException(nameof(verticalAccuracy));

            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            HorizontalAccuracy = horizontalAccuracy;
            VerticalAccuracy = verticalAccuracy;
        }
    }
}