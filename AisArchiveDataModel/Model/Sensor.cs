﻿using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A sensor in a <see cref="Device"/>. </summary>
    ///
    /// <remarks>
    ///     A device can have multiple <see cref="Sensor"/>s. For example, an on-body tracking device
    ///     could have a GPS sensor, an accelerometer, and a gyroscope.
    /// </remarks>
    public class Sensor : ArchiveBase
    {
        /// <summary> Gets or sets the type of the sensor. </summary>
        ///
        /// <remarks> Examples: Accelerometer, gyroscope, GPS. </remarks>
        /// 
        /// <value> The type of the sensor. </value>
        public string SensorType { get; set; }

        /// <summary> Gets or sets the seonsor's manufacturer. </summary>
        ///
        /// <value> The manufacturer; e.g., Texas Instruments. </value>
        public string Manufacturer { get; set; }

        /// <summary> Gets or sets the model name of the sensor. </summary>
        ///
        /// <value> The name model name. </value>
        public string ModelName { get; set; }

        /// <summary> Gets or sets the data channels that represent the sensor measurements. </summary>
        ///
        /// <value> The data channels. </value>
        public IEnumerable<IChannel> Channels { get; set; }
    }
}