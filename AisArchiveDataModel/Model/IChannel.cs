﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> Interface for (timeseries) data channels. </summary>
    ///
    /// <remarks>
    ///     Channels represents timeseries data. The data can be a numeric type, e.g., double, or it
    ///     could be a composite type, e.g., three acceleration values {x, y, z} per sample.
    ///     
    ///     As there is only one <see cref="UnitOfMeasurement"/> for each <see cref="IChannel"/>,
    ///     each channel is only allowed to represent one type of measurement. For example, it's OK
    ///     to have a channel that represents accelerometer readings, but you can't have a channel
    ///     that represents both GPS angles and elevation.
    /// </remarks>
    public interface IChannel
    {
        /// <summary> Gets the name of the channel. </summary>
        ///
        /// <value> The name. </value>
        string Name { get; }

        /// <summary> Gets the unit of measurement for each sample. </summary>
        ///
        /// <value> The unit of measurement. </value>
        UnitOfMeasurement UnitOfMeasurement { get; }

        /// <summary> Gets the time stamps for each sample. </summary>
        ///
        /// <value> The time stamps. </value>
        IEnumerable<TimeSpan> TimeStamps { get; }

        /// <summary> Gets the data. </summary>
        ///
        /// <returns> The data. </returns>
        IEnumerable Data { get; }
    }

    /// <summary> Generic interface for (timeseries) data channels. </summary>
    ///
    /// <remarks> As opposed to <see cref="IChannel"/> this interface returns typed samples. </remarks>
    ///
    /// <typeparam name="TData"> Type of the data. </typeparam>
    /// 
    /// ### <inheritdoc/>
    public interface IChannel<TData> : IChannel
    {
        /// <summary> Gets the sample values. </summary>
        ///
        /// <value> The sample values. </value>
        IEnumerable<TData> Values { get;  }

        /// <summary> Gets the time stamped samples. </summary>
        ///
        /// <value> The time stamped samples. </value>
        IEnumerable<TimeStampedSample<TData>> TimeStampedSamples { get; }
    }
}