﻿using System.ComponentModel;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> An actor. </summary>
    ///
    /// <remarks> Represents a "thing" in a system, such as an athlete, boat, etc. </remarks>
    public class Actor : ArchiveBase
    {
        /// <summary> Gets or sets the name of the actor. </summary>
        ///
        /// <remarks>
        ///     Allows the actor to be given a name. Examples include: athlete name, boat name, etc.
        ///     
        ///     Additional information about the actor should be added via the
        ///     <see cref="ArchiveBase.Tags"/> property. Examples include: Athlete AMS ID, boat size, etc.
        /// </remarks>
        ///
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the type of the actor. </summary>
        ///
        /// <remarks> Examples: Athlete, boat, etc. </remarks>
        ///
        /// <value> The type of the actor. </value>
        public string ActorType { get; set; }
    }
}