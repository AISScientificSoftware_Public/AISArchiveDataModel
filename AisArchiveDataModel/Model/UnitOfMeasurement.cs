﻿using System;
using Newtonsoft.Json;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A unit of measurement. </summary>
    ///
    /// <remarks>
    ///     This class uses the NuGet package "UnitsNet" to represent units of measurement. All recorded data
    ///     should have a corresponding unit of measurement to allow proper interpretation of archived data.
    /// </remarks>
    public class UnitOfMeasurement
    {
        private const string ExpectedUnitsLibraryNamespace = "UnitsNet.Units";

        [JsonIgnore]
        public static UnitOfMeasurement None = new UnitOfMeasurement();

        public readonly string QuantityType;    // Type of the quantity; e.g., AccelerationUnit
        public readonly string Units;   // The units of the measurement; e.g., MeterPerSecondSquared

        /// <summary> Constructor. </summary>
        ///
        /// <remarks>
        ///     This constructor takes an <see cref="Enum"/>, but it must be from the
        ///     <see cref="ExpectedUnitsLibraryNamespace"/> namespace, which is where the UnitsNet
        ///     package defines each of the units.
        /// </remarks>
        ///
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        ///     illegal values. </exception>
        ///
        /// <param name="unit"> An enum from the <see cref="ExpectedUnitsLibraryNamespace"/> namespace
        ///     that describes the unit of measure. </param>
        public UnitOfMeasurement(Enum unit)
        {
            var type = unit.GetType();
            if (type.Namespace != ExpectedUnitsLibraryNamespace)
                throw new ArgumentException($"{nameof(unit)} is not a valid unit type.");

            QuantityType = type.FullName;
            Units = unit.ToString();
        }

        [JsonConstructor]
        private UnitOfMeasurement(string quantityType, string units)
        {
            QuantityType = quantityType;
            Units = units;
        }

        /// <summary>
        ///     Private constructor that creates the None static instance.
        /// </summary>
        ///
        /// <remarks> Use the <see cref="None"/> property to get one of these. </remarks>
        private UnitOfMeasurement()
        {
            QuantityType = "None";
            Units = "None";
        }

        public bool IsNone() => IsNone(this);
        public static bool IsNone(UnitOfMeasurement unitOfMeasurement) => unitOfMeasurement == None;
    }
}