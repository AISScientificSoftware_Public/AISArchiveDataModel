﻿namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> Represents a location. </summary>
    ///
    /// <remarks>
    ///     The location can be a GeoCoordinate, e.g., the longitude, latitude, altitude of a
    ///     location, or it could represent a descriptive location, e.g., "left arm" of on athlete.
    /// </remarks>
    public class Location : ArchiveBase
    {
        public string Name { get; set; }

        public GeoCoordinate GeoCoordinate { get; set; }
    }
}