﻿using System;
using Newtonsoft.Json;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> Class for representing annotations. </summary>
    ///
    /// <remarks>
    ///     Annotations are used to indicate discrete events that have occurred, and can be used to
    ///     label moments or spans of time. The differ from <see cref="IChannel"/> objects as they
    ///     generally represent a stream of measurements.
    ///     
    ///     Examples annotations:
    ///         - Start of race (moment in time)
    ///         - Lap of race (span of time)  
    ///     
    ///     This class should be subclassed if extra properties are required for the annotation.
    /// </remarks>
    public class Annotation : ArchiveBase
    {
        /// <summary> Gets or sets the name of the annotation. </summary>
        ///
        /// <value> The name. </value>
        public string Name { get; set; }

        /// <summary> Gets or sets the start time of the annotation. </summary>
        ///
        /// <value> The start time. </value>
        public TimeSpan StartTime { get; set; }

        /// <summary> Gets or sets the duration of the event. </summary>
        ///
        /// <remarks> Defaults to TimeSpan.Zero. </remarks>
        /// 
        /// <value> The duration. </value>
        public TimeSpan Duration { get; set; }

        /// <summary> Gets the end time of the annotation. </summary>
        ///
        /// <value> The end time. </value>
        [JsonIgnore]
        public TimeSpan EndTime => StartTime + Duration;
    }
}