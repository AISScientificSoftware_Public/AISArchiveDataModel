﻿using Newtonsoft.Json;

namespace gov.ausport.AisArchiveDataModel
{
    public class ArchiveBase
    {
        /// <summary> Gets the tags. </summary>
        ///
        /// <remarks>
        ///     It is impossible to anticipate all of the metadata that needs to be collected in a system,
        ///     so we use a <see cref="TagCollection"/> to allow users to add additional metadata to the
        ///     information that is to be archived.
        ///     
        ///     Examples: Athlete AMS Id, list of equipment attached to a bike, etc.
        /// </remarks>
        ///
        /// <value> The tags. </value>
        public TagCollection Tags { get;  } = new TagCollection();

        protected ArchiveBase()
        {
        }

        /// <summary> Determine if we should serialize <see cref="Tags"/>. </summary>
        ///
        /// <remarks> Don't serialise <see cref="Tags"/> if the collection is empty. </remarks>
        ///
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ShouldSerializeTags()
        {
            return Tags.Count > 0;
        }
    }
}