﻿namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> Shared constants across the object model. </summary>
    public static class ModelConstants
    {

        /// <summary> Represents an unknown string value. </summary>
        /// 
        /// <remarks>
        ///     To be used when a property is mandatory, but unknown. Having this field
        ///     allows validation code to check that a user has entered a value and hasn't
        ///     simply forgotten to set it. That is, we make the unknown value explicit.
        /// </remarks>
        public static readonly string UnknownStringValue = "UNKNOWN_VALUE"; // The unknown string value
    }
}