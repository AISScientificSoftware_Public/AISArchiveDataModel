﻿using System;

namespace gov.ausport.AisArchiveDataModel
{
    /// <summary> A time stamped sample. </summary>
    ///
    /// <remarks> Time stamped samples have both a value and time associated with them. </remarks>
    ///
    /// <typeparam name="TData"> Type of the data. </typeparam>
    public class TimeStampedSample<TData>
    {
        /// <summary> Gets or sets the time that the sample was made. </summary>
        ///
        /// <value> The time. </value>
        public TimeSpan Time { get; set; }

        /// <summary> Gets or sets the value of the sample. </summary>
        ///
        /// <value> The value. </value>
        public TData Value { get; set; }

        public TimeStampedSample()
        {
        }

        public TimeStampedSample(TimeSpan time, TData value)
        {
            Time = time;
            Value = value;
        }
    }
}