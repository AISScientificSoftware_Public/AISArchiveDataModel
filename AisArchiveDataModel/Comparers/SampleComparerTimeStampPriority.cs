﻿using System;
using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel.Comparers
{
    public class SampleComparerTimeStampPriority<TData> : Comparer<TimeStampedSample<TData>>
        where TData : IComparable
    {
        public override int Compare(TimeStampedSample<TData> x, TimeStampedSample<TData> y)
        {
            var timeComparison = x.Time.CompareTo(y.Time);

            return timeComparison != 0
                ? timeComparison
                : x.Value.CompareTo(y.Value);
        }
    }
}