﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace gov.ausport.AisArchiveDataModel.Comparers
{
    /// <summary> A default channel equality comparer. </summary>
    ///
    /// <remarks>
    ///     Uses default equality comparer that only compares sample values.
    ///     
    ///     Subclass this if you want different comparison logic. For example, you may want to check
    ///     that the <see cref="IChannel.UnitOfMeasurement"/> matches on each channel.
    /// </remarks>
    ///
    /// <typeparam name="TData"> Type of the data. </typeparam>
    ///
    /// ### <inheritdoc/>
    public class DefaultChannelEqualityComparer<TData> : EqualityComparer<IChannel<TData>>
        where TData : IEquatable<TData>
    {
        // TODO: Custom comparison logic could be implemented via a delegate.
        // - Could be passed in via constructor or set in a property.
        // - Subclassing is easier if a wide range of logic isn't necessary.

        public override bool Equals(IChannel<TData> x, IChannel<TData> y)
        {
            return AllSamplesAreEqual(x, y);
        }

        public override int GetHashCode(IChannel<TData> obj)
        {
            throw new NotImplementedException();
        }

        protected bool AllSamplesAreEqual(IChannel<TData> x, IChannel<TData> y)
        {
            var equalityComparer = new SampleEqualityComparer<TData>();
            return x.TimeStampedSamples.SequenceEqual(y.TimeStampedSamples, equalityComparer);
        }
    }
}