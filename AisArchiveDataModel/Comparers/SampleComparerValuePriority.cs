﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gov.ausport.AisArchiveDataModel.Comparers
{
    public class SampleComparerValuePriority<TData> : Comparer<TimeStampedSample<TData>>
        where TData : IComparable
    {
        public override int Compare(TimeStampedSample<TData> x, TimeStampedSample<TData> y)
        {
            var valueComparison = x.Value.CompareTo(y.Value);

            return valueComparison != 0
                ? valueComparison
                : x.Time.CompareTo(y.Time);
        }
    }
}
