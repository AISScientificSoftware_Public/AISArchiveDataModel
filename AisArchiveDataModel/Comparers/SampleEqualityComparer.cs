﻿using System;
using System.Collections.Generic;

namespace gov.ausport.AisArchiveDataModel.Comparers
{
    public class SampleEqualityComparer<TData> : EqualityComparer<TimeStampedSample<TData>>
        where TData : IEquatable<TData>
    {
        public override bool Equals(TimeStampedSample<TData> x, TimeStampedSample<TData> y)
        {
            return x.Time == y.Time && x.Value.Equals(y.Value);
        }

        public override int GetHashCode(TimeStampedSample<TData> obj)
        {
            return obj.Time.GetHashCode() ^ obj.Value.GetHashCode();
        }
    }
}