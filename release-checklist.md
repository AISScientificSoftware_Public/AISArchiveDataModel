1. Update `release-notes.md`.
2. Open Package tab in Project properties and:
   - Copy latest release information to Package tab in Project properties.
   - Bump package, assembly, and file versions.
3. Publish NuGet package locally.
4. Push to GitLab.