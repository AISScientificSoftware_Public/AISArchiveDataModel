# AIS Archive Data Model

This project provides a C# data model that is to be used for *archive data storage* in the AIS *Performance Data Platform*. The platform is a set of Azure services that are used to store, validate, and analyse captured data. The platform also allows download of stored data, as well as providing access to analytics results.

The remainder of this document serves as a basic introduction to the Performance Data Platform and its archive storage component. Hence, this document is not meant to be comprehensive; its purpose it to give users a general picture of the model's code and the thoughts behind its design.

## Performance Data Platform

A very high level view of the platform is presented below. The archive block is highlighted to make it stand out.

```mermaid
graph TD;
	ud[Uploaded data]
	ud --> val[Validation]
	val --> arch[Archive]
	val --> an[Analytics]
	an --> met[Metrics database]
	style arch fill:#ff7	
```

Other configurations of the platform are also possible, but the figure above gives a reasonable general illustration of the concept.

### Archive storage

The archive is meant to act as a long-term repository for collected data, meaning it has to be readily understood years after it is stored. Our approach is to make the data self-describing. For example, a series of regularly sampled measurements should store the name of the series, the units it is stored in, the sample rate, etc.

#### Data ingress and egress

Note that we don't mandate that applications use this data model internally. Nor do we mandate that data is uploaded to Azure in this data model format. The only requirement is that data is archived in this format. Where the data is transformed into this model's representation is project-specific decision, and will be dictated by a series of factors that cannot be mandated; e.g., the source of the data, data transfer requirements, availability of key metrics, etc.

Accordingly, we expect that users who download archived data will transform it to a form that suits their needs, if necessary. This could be done in the consuming application, an Azure "download API", or any other place that is appropriate. Again, this is a project-specific decision.

## Data model

This data model project provides both a description of the archive data format, as well as some simple tools to work with it. The following provides a brief overview of the data model.

### Model hierarchy

A single collection of captured data forms a `Session` Sessions are the top-level of the model hierarchy, with examples including: a race, a training session, an equipment calibration run, etc. The complete object hierarchy is illustrated below:

```mermaid
graph TD;
	Session
	Session --> Location
	Location --> GeoCoordinate
	Session --> Devices
	Session --> Events
	Devices --> Actor
	Devices --> Sensors
	Sensors --> Channels
	Channels --> UnitOfMeasurement
	Channels --> TimeStamps
	Channels --> Data
```

### Model data, metadata, and tags

The model provides an easy way to store data and metadata that are expected to be common to most projects via object properties, but it also provides a way to provide additional data/metadata via a tagging mechanism.

Most model objects inherit from `ArchiveBase`, which has a `TagCollection` for attaching additional information to objects. The `TagCollection` is a wrapper to `IDictionary<string, object>`, hence tags can represent any type of `object`, and are stored/retrieved via `string` keys.

### Channels

Most high-volume sports data is time series data, which the model stores in derivations of `IChannel`, namely `SampledChannel` and `TimeStampedChannel`. These objects are designed to store regularly and irregularly sampled data, respectively, and do not store metadata on a per-sample level for (storage space) efficiency.

Accelerometer samples are a good example of data that would be stored in an `IChannel` object.

### Events

The model uses the `Annotation` class to store low-volume session data as "events". Examples could include the start of a race, a specific lap in a race, or interesting occurrences that a user has tagged via some GUI application.

### Serialisation

The model objects have been designed to be serialisable for storage in the archive. It is expected that the objects will be serialised to either JSON or BSON for storage. Compression is optional.

These decisions will be made on a project-by-project basis.

## Examples

Please see the `AisArchiveDataModelTests` project within the solution for examples and unit tests that illustrate how to use the model (in the `AisArchiveDataModel` project).

