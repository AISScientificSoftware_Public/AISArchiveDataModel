﻿using System;
using System.Linq;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using NUnit.Framework;
using UnitsNet.Units;

namespace AisArchiveDataModelTests.Examples
{
    /// <summary> (Unit Test Fixture) Example composite acceleration sample. </summary>
    ///
    /// <remarks>
    ///     Each of the channels have the same unit, so we can combine the channel samples into a
    ///     composite sample.
    /// </remarks>
    internal class AccelerationSample
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Z { get; set; }

        public AccelerationSample(int x, int y, int z)
        {
            X = x;
            Y = y;
            Z = z;
        }
    }

    [TestFixture]
    public class ChannelExamples
    {
        private const double SampleRate = 100;
        private readonly Enum _units = AccelerationUnit.MeterPerSecondSquared;

        [Test]
        public void Example_SampledChannel_SeparateChannelPerAxis()
        {
            /*
             * Saves each accelerometer axis in a separate channel.
             * - More space efficient when serialised.
             * - Can be more difficult to use the deserialised object, and may require mapping
             *   back to the application model.
             */
            var acclerationX = new SampledChannel<int>("AccelerationX", _units, SampleRate, new[] { 1, 2, 3 });
            var acclerationY = new SampledChannel<int>("AccelerationY", _units, SampleRate, new[] { 4, 5, 6 });
            var acclerationZ = new SampledChannel<int>("AccelerationZ", _units, SampleRate, new[] { 7, 8, 9 });

            var channels = new[] {acclerationX, acclerationY, acclerationZ};
            channels.Dump();
        }

        [Test]
        public void Example_SampledChannel_CompositeChannel()
        {
            /*
             * Saves each sample as a composite of {x, y, z} channels.
             * - Takes up more space when serialised.
             * - Can be easier to use as it forces alignment of the samples.
             */
            var data = new[] {
                new AccelerationSample(1, 2, 3),
                new AccelerationSample(4, 5, 6),
                new AccelerationSample(7, 8, 9),
            };

            var acceleration = new SampledChannel<AccelerationSample>(
                "Acceleration",
                _units,
                SampleRate,
                data);

            var channels = new[] {acceleration};
            channels.Dump();

            var xData = acceleration
                .Values
                .Select(sample => sample.X);
            Assert.AreEqual(new[] {1, 4, 7}, xData);
        }

        [Test]
        public void Example_TimeStampedChannel()
        {
            /*
             * Shows how to create a TimeStampedChannel that has irregularly sampled data.
             */
            var data = new[] {
                new TimeStampedSample<int>(TimeSpan.FromSeconds(0.0), 0),
                new TimeStampedSample<int>(TimeSpan.FromSeconds(2.5), 1),
                new TimeStampedSample<int>(TimeSpan.FromSeconds(3.1), 2),
            };

            var acceleration = new TimeStampedChannel<int>("Acceleration", _units, data);
            acceleration.Dump();
        }
    }
}