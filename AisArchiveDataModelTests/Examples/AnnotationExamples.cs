﻿using System;
using System.Collections.Generic;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AisArchiveDataModelTests.Examples
{
    /// <summary> A lap annotation. </summary>
    ///
    /// <remarks> Example of a subclassed annotation. </remarks>
    ///
    /// ### <inheritdoc/>
    internal class LapAnnotation : Annotation
    {
        public int Number { get; set; }
        public List<TimeSpan> Splits { get; set; }
    }

    [TestFixture]
    public class AnnotationExamples
    {
        [Test]
        public void Example_Annotations_SessionWithMixedAnnotations()
        {
            /*
             * A basic example that shows how to add annotations to a session.
             * 
             * It shows that annotations can be discrete events or represent a period of
             * time. Additional data can also be added to annotations by subclassing them.
             */
            var session = new Session {
                StartTime = new DateTime(2017, 1, 1, 12, 0, 0),
                Events = new[] {
                    new Annotation {
                        Name = "Race start",
                        StartTime = TimeSpan.FromSeconds(5),
                    },
                    new LapAnnotation {
                        Name = "Lap 1",
                        StartTime = TimeSpan.FromSeconds(100),
                        Duration = TimeSpan.FromSeconds(50),
                        Number = 1,
                        Splits = new List<TimeSpan> {
                            TimeSpan.FromSeconds(15),
                            TimeSpan.FromSeconds(15),
                            TimeSpan.FromSeconds(20),
                        }
                    },
                }
            };

            JsonConvert.SerializeObject(session).Dump();
            Assert.Pass();
        }
    }
}