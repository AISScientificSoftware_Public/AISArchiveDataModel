﻿using Newtonsoft.Json;
using NUnit.Framework;

namespace AisArchiveDataModelTests.Utility
{
    public static class SerialisationHelpers
    {
        public static void CheckDeserialisedObjectMatchesOriginal<T>(this T original)
        {
            var serialised = JsonConvert.SerializeObject(original);
            var deserialised = JsonConvert.DeserializeObject<T>(serialised);

            // Output serialised objects for visual testing
            original.Dump();
            deserialised.Dump();

            // Check objects match
            Assert.AreEqual(serialised, JsonConvert.SerializeObject(deserialised));
        }
    }
}