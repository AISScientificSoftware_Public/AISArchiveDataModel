﻿using System;
using Newtonsoft.Json;

namespace AisArchiveDataModelTests.Utility
{
    internal static class OutputHelpers
    {
        /// <summary> An object extension method that dumps the given object to the console. </summary>
        ///
        /// <remarks>
        ///     This method is used to show what the given object will look like once it has been
        ///     serialised.
        /// </remarks>
        ///
        /// <param name="obj"> The obj to act on. </param>
        internal static void Dump(this object obj)
        {
            Console.WriteLine(
                JsonConvert.SerializeObject(obj, Formatting.Indented)
            );
        }
    }
}
