﻿using System;
using gov.ausport.AisArchiveDataModel;
using gov.ausport.AisArchiveDataModel.Comparers;
using NUnit.Framework;

namespace AisArchiveDataModelTests.UnitTests.Comparers
{
    [TestFixture]
    public class SampleEqualityComparerTests
    {
        [Test]
        public void Equals_SameValues_ReturnsTrue()
        {
            var comparer = new SampleEqualityComparer<int>();

            Assert.IsTrue(
                comparer.Equals(
                    new TimeStampedSample<int>(TimeSpan.Zero, 0),
                    new TimeStampedSample<int>(TimeSpan.Zero, 0)));
            Assert.IsTrue(
                comparer.Equals(
                    new TimeStampedSample<int>(TimeSpan.FromSeconds(10), 20),
                    new TimeStampedSample<int>(TimeSpan.FromSeconds(10), 20)));
        }

        [Test]
        public void Equals_DifferentValues_ReturnsFalse()
        {
            var comparer = new SampleEqualityComparer<int>();

            Assert.IsFalse(
                comparer.Equals(
                    new TimeStampedSample<int>(TimeSpan.Zero, 0),
                    new TimeStampedSample<int>(TimeSpan.Zero, 1)));
            Assert.IsFalse(
                comparer.Equals(
                    new TimeStampedSample<int>(TimeSpan.Zero, 0),
                    new TimeStampedSample<int>(TimeSpan.MaxValue, 0)));
        }
    }
}