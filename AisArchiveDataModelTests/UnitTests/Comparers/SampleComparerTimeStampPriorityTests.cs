﻿using System;
using gov.ausport.AisArchiveDataModel;
using gov.ausport.AisArchiveDataModel.Comparers;
using NUnit.Framework;

namespace AisArchiveDataModelTests.UnitTests.Comparers
{
    [TestFixture]
    public class SampleComparerTimeStampPriorityTests
    {
        private readonly TimeStampedSample<int> _fixedSample = new TimeStampedSample<int>(TimeSpan.FromSeconds(1), 1);

        [Test]
        [TestCase(1, 1, 0)] // Samples are equal => 0
        [TestCase(2, 1, 1)] // t1 > t2 => 1
        [TestCase(0, 1, -1)] // t1 < t2 => -1
        [TestCase(1, 2, 1)] // TimeStamps same, v1 > v2 => 1
        [TestCase(1, 0, -1)] // TimeStamps same, v1 < v2 => -1
        public void Compare_TimeStampedSamples_TimeStampValueIsGivenPriorityInComparison(int timeStamp, int value, int expected)
        {
            var comparer = new SampleComparerTimeStampPriority<int>();
            var testSample = new TimeStampedSample<int>(TimeSpan.FromSeconds(timeStamp), value);

            Assert.AreEqual(expected, comparer.Compare(testSample, _fixedSample));
        }
    }
}