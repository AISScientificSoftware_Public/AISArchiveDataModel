﻿using System;
using gov.ausport.AisArchiveDataModel;
using gov.ausport.AisArchiveDataModel.Comparers;
using NUnit.Framework;
using UnitsNet.Units;

namespace AisArchiveDataModelTests.UnitTests.Comparers
{
    [TestFixture]
    public class ChannelEqualityComparerTests
    {
        [Test]
        [TestCase(1, 1, 1, 1, true)] // Equal
        [TestCase(0, 1, 1, 1, false)] // Times differ
        [TestCase(1, 0, 1, 1, false)] // Values differ
        public void Equals_TimeStampedSamples_ReturnsTrueIfSamplesAreEqual(int t1, int v1, int t2, int v2, bool expected)
        {
            var comparer = new DefaultChannelEqualityComparer<int>();
            var dummySample1 = new TimeStampedSample<int>(TimeSpan.FromSeconds(100), 100);
            var dummySample2 = new TimeStampedSample<int>(TimeSpan.FromSeconds(200), 200);

            var channel1 = new TimeStampedChannel<int>(
                "channel1",
                AccelerationUnit.MeterPerSecondSquared,
                new [] {
                    dummySample1,
                    dummySample2,
                    new TimeStampedSample<int>(TimeSpan.FromSeconds(t1), v1),
                });
            var channel2 = new TimeStampedChannel<int>(
                "channel2",
                AccelerationUnit.MeterPerSecondSquared,
                new [] {
                    dummySample1,
                    dummySample2,
                    new TimeStampedSample<int>(TimeSpan.FromSeconds(t2), v2),
                });

            Assert.AreEqual(expected, comparer.Equals(channel1, channel2));
        }
    }
}