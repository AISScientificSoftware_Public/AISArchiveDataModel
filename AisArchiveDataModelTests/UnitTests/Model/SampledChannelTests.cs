﻿using System;
using System.Collections.Generic;
using System.Linq;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using gov.ausport.AisArchiveDataModel.Comparers;
using Newtonsoft.Json;
using NUnit.Framework;
using UnitsNet.Units;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class SampledChannelTests
    {
        private const double SampleRate = 10;
        private const double StartOffset = 1.5; // seconds

        [Test]
        public void Data_SampledChannel_ReturnsData()
        {
            var channel = CreateChannel();
            var expectedData = CreateData();
            
            CollectionAssert.AreEqual(expectedData, channel.Data);
        }

        [Test]
        public void Values_SampledChannel_ReturnsData()
        {
            var channel = CreateChannel();
            var expectedData = CreateData();
            
            CollectionAssert.AreEqual(expectedData, channel.Values);
        }

        [Test]
        public void TimeStamps_SampledChannel_ReturnsTimes()
        {
            var channel = CreateChannel();
            var expectedTimes = CreateExpectedTimes(channel.Values.Count());

            CollectionAssert.AreEqual(expectedTimes, channel.TimeStamps);

            IEnumerable<TimeSpan> CreateExpectedTimes(int count)
            {
                var i = 0;
                while (i < count) {
                    yield return TimeSpan.FromSeconds(StartOffset + i++ / SampleRate);
                }
            }
        }

        [Test]
        public void GetTimeStampedSamples_SampledChannel_ReturnsTimeStampedSamples()
        {
            var channel = CreateChannel();
            var expectedData = CreateData().ToArray();
            var expectedSamples = CreateExpectedSamples(expectedData.Length).ToArray();

            CollectionAssert.AreEqual(expectedSamples, channel.TimeStampedSamples, new SampleComparerTimeStampPriority<int>());
            
            IEnumerable<TimeStampedSample<int>> CreateExpectedSamples(int count)
            {
                var data = CreateData().ToArray();

                var i = 0;
                while (i < count) {
                    yield return new TimeStampedSample<int>() {
                        Time = TimeSpan.FromSeconds(StartOffset + i / SampleRate),
                        Value = data[i++]
                    };
                }
            }
        }

        [Test]
        public void SampledChannel_SerialisationAndDeserialisation_DeserialisedObjectMatchesOriginal()
        {
            var channel = CreateChannel();

            channel.CheckDeserialisedObjectMatchesOriginal();
        }

        #region Utility

        private static IEnumerable<int> CreateData()
        {
            return Enumerable.Range(1, 3);
        }

        private static SampledChannel<int> CreateChannel()
        {
            const double sampleRate = 10;
            const double startOffset = 1.5; // seconds

            return new SampledChannel<int>(
                "Test",
                AccelerationUnit.MeterPerSecondSquared,
                sampleRate,
                CreateData()) {
                StartTimeOffset = TimeSpan.FromSeconds(startOffset),
            };
        }

        #endregion
    }
}