using System;
using gov.ausport.AisArchiveDataModel;
using NUnit.Framework;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class GeoCoordinateTests
    {
        [Test]
        public void Unknown_DifferentGeoCoordinates_ReferenceSameObject()
        {
            var gc1 = GeoCoordinate.Unknown;
            var gc2 = GeoCoordinate.Unknown;

            Assert.AreSame(gc1, gc2);
        }

        [Test]
        public void IsUnknown_UnknownGeoCoordinate_ReturnsTrue()
        {
            Assert.IsTrue(GeoCoordinate.Unknown.IsUnknown);
        }

        [Test]
        public void IsUnknown_ValidGeoCoordinate_ReturnsFalse()
        {
            Assert.IsFalse(new GeoCoordinate(0, 0).IsUnknown);
        }

        [Test]
        [TestCase(-181, 0)]
        [TestCase(181, 0)]
        [TestCase(0, -91)]
        [TestCase(0, 91)]
        [TestCase(double.PositiveInfinity, 0)]
        [TestCase(0, double.PositiveInfinity)]
        [TestCase(0, 0, 0.0, -1.0)]
        [TestCase(0, 0, 0.0, 0.0, -1.0)]
        public void Constructor_OutOfRangeCoordinate_ArgumentOutOfRangeExceptionIsThrown(
            double longitude,
            double latitude,
            double? altitude = null,
            double? horizontalAccuracy = null,
            double? verticalAccuracy = null)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => {
                var _ = new GeoCoordinate(longitude, latitude, altitude, horizontalAccuracy, verticalAccuracy);
            });
        }

        [Test]
        [TestCase(double.NaN, 0)]
        [TestCase(0, double.NaN)]
        [TestCase(0, 0, double.NaN)]
        [TestCase(0, 0, double.PositiveInfinity)]
        [TestCase(0, 0, 0.0, double.NaN)]
        [TestCase(0, 0, 0.0, double.PositiveInfinity)]
        [TestCase(0, 0, 0.0, 0.0, double.NaN)]
        [TestCase(0, 0, 0.0, 0.0, double.PositiveInfinity)]
        public void Constructor_InvalidCoordinateValue_ArgumentExceptionIsThrown(
            double longitude,
            double latitude,
            double? altitude = null,
            double? horizontalAccuracy = null,
            double? verticalAccuracy = null)
        {
            Assert.Throws<ArgumentException>(() => {
                var _ = new GeoCoordinate(longitude, latitude, altitude, horizontalAccuracy, verticalAccuracy);
            });
        }
    }
}
