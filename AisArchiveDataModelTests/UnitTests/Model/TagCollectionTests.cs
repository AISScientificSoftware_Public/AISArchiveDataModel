﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class TagCollectionTests
    {
        [Test]
        public void Add_InvalidKey_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentNullException>(() => {
                var _ = new TagCollection {{"test", null}};
            });
            Assert.Throws<ArgumentException>(() => {
                var _ = new TagCollection {{"test", " "}};
            });
            Assert.Throws<ArgumentException>(() => {
                var _ = new TagCollection {{null, 1}};
            });
            Assert.Throws<ArgumentException>(() => {
                var _ = new TagCollection {{" ", 1}};
            });
        }

        [Test]
        public void TagCollection_SerialisationAndDeserialisation_DeserialisedObjectMatchesOriginal()
        {
            var tags = CreateTestCollection();

            tags.CheckDeserialisedObjectMatchesOriginal();
        }

        private static TagCollection CreateTestCollection()
        {
            return new TagCollection {
                {"numeric", 1},
                {"string", "one"},
            };
        }
    }
}