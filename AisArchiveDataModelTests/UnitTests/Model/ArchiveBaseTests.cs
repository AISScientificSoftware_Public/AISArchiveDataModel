﻿using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using Newtonsoft.Json;
using NUnit.Framework;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class ArchiveBaseTests
    {
        [Test]
        public void Actor_WithTags_DeserialisedObjectMatchesOriginal()
        {
            var actor = CreateActor();
            actor.Tags.Add("test", 1);

            actor.CheckDeserialisedObjectMatchesOriginal();
        }

        [Test]
        public void Actor_WithoutTags_DeserialisedObjectMatchesOriginal()
        {
            var actor = CreateActor();

            actor.CheckDeserialisedObjectMatchesOriginal();
        }

        private static Actor CreateActor()
        {
            var actor = new Actor {
                Name = "123456",
                ActorType = "Athlete",
            };
            return actor;
        }
    }
}