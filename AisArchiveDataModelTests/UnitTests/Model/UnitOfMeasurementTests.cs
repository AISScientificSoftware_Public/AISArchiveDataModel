﻿using System;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using Newtonsoft.Json;
using NUnit.Framework;
using UnitsNet.Units;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class UnitOfMeasurementTests
    {
        [Test]
        public void Constructor_ValidParameter_CreatesValidUnitOfMeasurement()
        {
            const string expectedQuantityType = "UnitsNet.Units.AccelerationUnit";
            const string expectedUnits = "MeterPerSecondSquared";

            var unitOfMeasurement = new UnitOfMeasurement(
                UnitsNet.Units.AccelerationUnit.MeterPerSecondSquared);

            Assert.AreEqual(expectedQuantityType, unitOfMeasurement.QuantityType);
            Assert.AreEqual(expectedUnits, unitOfMeasurement.Units);
        }

        [Test]
        public void Constructor_InvalidParameter_ThrowsException()
        {
            Assert.Throws<ArgumentException>(() => {
                var unitOfMeasurement = new UnitOfMeasurement(DateTimeKind.Local);
            });
        }

        [Test]
        public void None_WhenComparingToAnotherNone_ReferencesAreEqual()
        {
            var u1 = UnitOfMeasurement.None;
            var u2 = UnitOfMeasurement.None;

            Assert.AreSame(u1, u2);
        }

        [Test]
        public void IsNone_None_ReturnsTrue()
        {
            var none = UnitOfMeasurement.None;

            Assert.True(none.IsNone());
            Assert.True(UnitOfMeasurement.IsNone(none));
        }

        [Test]
        public void IsNone_NotNone_ReturnsTrue()
        {
            var notNone = new UnitOfMeasurement(AccelerationUnit.MeterPerSecondSquared);

            Assert.False(notNone.IsNone());
            Assert.False(UnitOfMeasurement.IsNone(notNone));
        }

        [Test]
        public void UnitOfMeasurement_SerialisationAndDeserialisation_DeserialisedObjectMatchesOriginal()
        {
            var unitOfMeasurement = new UnitOfMeasurement(
                UnitsNet.Units.AccelerationUnit.MeterPerSecondSquared);

            unitOfMeasurement.CheckDeserialisedObjectMatchesOriginal();
        }
    }
}