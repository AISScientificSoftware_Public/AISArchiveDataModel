﻿using System;
using System.Linq;
using AisArchiveDataModelTests.Utility;
using gov.ausport.AisArchiveDataModel;
using gov.ausport.AisArchiveDataModel.Comparers;
using Newtonsoft.Json;
using NUnit.Framework;
using UnitsNet.Units;

namespace AisArchiveDataModelTests.UnitTests.Model
{
    [TestFixture]
    public class TimeStampedChannelTests
    {
        [Test]
        public void TimeStampedChannel_SerialisationAndDeserialisation_DeserialisedObjectMatchesOriginal()
        {
            var data = new[] {
                new TimeStampedSample<int>(TimeSpan.FromSeconds(0), 1),
                new TimeStampedSample<int>(TimeSpan.FromSeconds(1), 3),
                new TimeStampedSample<int>(TimeSpan.FromSeconds(2), 5),
            };

            var timeStampedChannel = new TimeStampedChannel<int>(
                "Test",
                AccelerationUnit.MeterPerSecondSquared,
                data.ToArray());

            timeStampedChannel.CheckDeserialisedObjectMatchesOriginal();
        }
    }
}