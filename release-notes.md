# Release notes

## [2018/01/12] 1.0.0-alpha1

Initial alpha release for comments and feedback.

## [2018/02/13] 1.0.0-alpha2

Added UnitOfMeasurement.None.

